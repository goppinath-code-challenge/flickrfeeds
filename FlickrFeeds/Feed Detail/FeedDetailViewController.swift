//
//  FeedDetailViewController.swift
//  FlickrFeeds
//
//  Created by Goppinath Thurairajah on 08.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

class FeedDetailViewController: UIViewController {
    
    var imageEntry: FlickrFeed.ImageEntry! // Mut be there
    
    var imageCacheManager: FeedViewModel.ImageCacheManager?
    
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var publishedDateLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    
    @IBOutlet weak var otherPhotosCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        title = imageEntry.title
        
        baseView.setShadow()
        
        prepareUI()
    }
    
    private func prepareUI() {
        
        imageCacheManager?.loadImage(for: imageEntry) { [weak self] image in
            
            self?.previewImageView.image = image
        }
        
        titleLabel.text = imageEntry.title
        
        authorNameLabel.text = imageEntry.authors?.first?.name ?? "Unknown"
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = .medium
    
        publishedDateLabel.text = dateFormatter.string(from: imageEntry.published)
        
        tagsLabel.text = imageEntry.categories?.map { return "#\($0.term)" }.joined(separator: ", ") ?? "Unknown"
    }

    // MARK:- Actions
    
    @IBAction func gridViewBBITapped(_ sender: UIBarButtonItem) {
        
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FeedDetailViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 24
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherPhotosCollectionViewCellIndentifier", for: indexPath)
        
        return cell
    }
}
