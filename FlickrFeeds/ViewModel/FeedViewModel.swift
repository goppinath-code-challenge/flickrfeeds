//
//  FeedViewModel.swift
//  FlickrFeeds
//
//  Created by Goppinath Thurairajah on 07.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

import FeedKit

class FeedViewModel: NSObject {

    private var imageEntryStore = [FlickrFeed.ImageEntry]()

    lazy var imageCacheManager = ImageCacheManager()
    
    func loadKittens(tags: Set<String>? = nil, completionQueue: DispatchQueue? = nil, completion: (() -> Void)? = nil) {
        
//        guard let feedURL = FlickrFeed.publicPhotosURL(tags: ["kittens"]) else { return }
        
        guard let feedURL = FlickrFeed.publicPhotosURL(tags: tags) else { return }

        FeedParser(URL: feedURL).parseAsync(queue: DispatchQueue.global(qos: .userInitiated)) { [weak self] result in
            
            switch result {
            case .failure(let error): print(error)
            case .success(let feed):
                
                self?.imageEntryStore = feed.atomFeed?.entries?.compactMap { FlickrFeed.ImageEntry(atomFeedEntry: $0) } ?? []
                
                if let completionQueue = completionQueue { completionQueue.sync { completion?() } }
                else { completion?() }
            }
        }
    }
}

// MARK:- Collection View DataSource
extension FeedViewModel {
    
    func numberOfSections() -> Int {
        
        return 1
    }
    
    func numberOfItems(in section: Int) -> Int {
        
        return imageEntryStore.count
    }
    
    func imageEntry(at: IndexPath) -> FlickrFeed.ImageEntry {
        
        return imageEntryStore[at.item]
    }
}

extension FeedViewModel {
    
    class ImageCacheManager {
        
        private lazy var carImageCache = NSCache<NSString, UIImage>()
        
        private func load(imageURL url: URL,  completion: @escaping (UIImage?) -> ()) {
            
            URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                
                if let data = data {
                    
                    if let image = UIImage(data: data) {
                
                        completion(image)
                    }
                    else {
                        
                        // TODO: What if the data cannot be converted to UIImage
                        completion(nil)
                    }
                }
                else {
                    
                    // TODO: Handle network errors
                    completion(nil)
                }
            }.resume()
        }
        
        func loadImage(for imageEntry: FlickrFeed.ImageEntry, completion: @escaping (UIImage) -> ()) {
            
            if let carImage = carImageCache.object(forKey: imageEntry.id as NSString) {
                
                 completion(carImage)
            }
            else {
                
                load(imageURL: imageEntry.imageURL) { [weak self] image in
                    
                    DispatchQueue.main.async {  completion(image ?? #imageLiteral(resourceName: "placeholder")) }
                    
                    if let image = image { self?.carImageCache.setObject(image, forKey: imageEntry.id as NSString) }
                }
            }
        }
    }
}
