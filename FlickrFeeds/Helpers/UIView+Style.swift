//
//  UIView+Style.swift
//  FlickrFeeds
//
//  Created by Goppinath Thurairajah on 08.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

extension UIView {
    
    func setShadow() {
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.75
        self.layer.shadowColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
    }
}
