//
//  FeedCollectionViewController.swift
//  FlickrFeeds
//
//  Created by Goppinath Thurairajah on 08.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class FeedCollectionViewController: UICollectionViewController {

    let emptyDataSourceMessage = "No data available, may be try with different tags."
    
    @IBOutlet var viewModel: FeedViewModel!
    
    fileprivate lazy var searchController: UISearchController = {
        
        let searchController = UISearchController(searchResultsController: nil)
        
        searchController.hidesNavigationBarDuringPresentation = false
        
        searchController.obscuresBackgroundDuringPresentation = false
        
        return searchController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
//        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
        searchController.searchResultsUpdater = self
        
        definesPresentationContext = true
        
        navigationItem.searchController = searchController
        
        searchController.searchBar.text = "bird"
        
        loadKittens()
    }
    
    func loadKittens() {
        
        let tagsArray = searchController.searchBar.text?.split(separator: " ").compactMap { String($0) }
        
        var tagsSet: Set<String>? = nil
        
        if let tagsArray = tagsArray { tagsSet = Set(tagsArray) }
        
        viewModel.loadKittens(tags: tagsSet, completionQueue: DispatchQueue.main) { [weak self] in
            
            self?.collectionView.reloadData()
            
            self?.collectionView.presentEmptyDataSource(message: self?.emptyDataSourceMessage ?? "")
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        switch segue.destination {
        case let feedDetailViewController as FeedDetailViewController:
            
            if let indexPathForSelectedItem = collectionView.indexPathsForSelectedItems?.first {
                
                feedDetailViewController.imageEntry = viewModel.imageEntry(at: indexPathForSelectedItem)
                feedDetailViewController.imageCacheManager = viewModel.imageCacheManager
            }
            
        default: break
        }
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return viewModel.numberOfSections()
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return viewModel.numberOfItems(in: section)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeedCollectionViewCell.cellIdentifier, for: indexPath) as! FeedCollectionViewCell
    
        // Configure the cell
    
        let imageEntry = viewModel.imageEntry(at: indexPath)

        cell.decorate(imageEntry: imageEntry)
        
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let imageEntry = viewModel.imageEntry(at: indexPath)
        
        viewModel.imageCacheManager.loadImage(for: imageEntry) { image in
            
            (cell as? FeedCollectionViewCell)?.imageView.image = image
        }
    }

}

extension FeedCollectionViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {

        loadKittens()
    }
}
