//
//  FeedCollectionViewCell.swift
//  FlickrFeeds
//
//  Created by Goppinath Thurairajah on 08.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import UIKit

class FeedCollectionViewCell: UICollectionViewCell {
 
    static let cellIdentifier = "FeedCollectionViewCellIdentifier"

    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        baseView.layer.borderWidth = 0.5
        baseView.layer.borderColor = #colorLiteral(red: 0.8587508798, green: 0.858871758, blue: 0.8587127328, alpha: 1)
    }
    
    func decorate(imageEntry: FlickrFeed.ImageEntry) {
        
        titleLabel.text = imageEntry.title
    }
}
