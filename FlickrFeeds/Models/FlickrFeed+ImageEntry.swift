//
//  FlickrFeed+ImageEntry.swift
//  FlickrFeeds
//
//  Created by Goppinath Thurairajah on 08.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation

import FeedKit

extension FlickrFeed {
    
    struct ImageEntry {
        
        struct Author {
            
            let name: String
            
            init?(feedEntryAuthor: AtomFeedEntryAuthor) {
                
                guard let name = feedEntryAuthor.name, !name.isEmpty else { return nil }
                
                self.name = name
            }
        }
        
        struct Category {
            
            let term: String
            
            init?(feedEntryCategory: AtomFeedEntryCategory) {
                
                guard let term = feedEntryCategory.attributes?.term, !term.isEmpty else { return nil }
                
                self.term = term
            }
        }

        
        let id: String
        let title: String
        let imageURL: URL
        let published: Date
        
        let authors: [Author]?
        let categories: [Category]?
        
        init?(atomFeedEntry: AtomFeedEntry) {
        
            let oImageURLString = atomFeedEntry.links?.filter { ($0.attributes?.type?.contains("image/") ?? false) }.first?.attributes?.href
            
            guard
                let id = atomFeedEntry.id,
                let title = atomFeedEntry.title,
                let imageURLString = oImageURLString,
                let imageURL = URL(string: imageURLString),
                let published = atomFeedEntry.published
            else  { return nil }
            
            self.id = id
            self.title = title
            self.imageURL = imageURL
            self.published = published
            
            self.authors = atomFeedEntry.authors?.compactMap { Author(feedEntryAuthor: $0) }
            self.categories = atomFeedEntry.categories?.compactMap { Category(feedEntryCategory: $0) }
        }
    }
}
