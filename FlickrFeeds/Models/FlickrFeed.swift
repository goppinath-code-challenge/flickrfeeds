//
//  FlickrFeed.swift
//  FlickrFeeds
//
//  Created by Goppinath Thurairajah on 07.12.19.
//  Copyright © 2019 Goppinath Thurairajah. All rights reserved.
//

import Foundation

struct FlickrFeed {
    
    enum ErrorType: Error {
        
        case unknown
    }
    
    enum ResourceType: String {
        
        case publicPhotos = "photos_public.gne"
    }
    
    enum PublicPhotosQueryItemType: String {
        
        case tags = "tags"
    }
    
    static var urlComponents: URLComponents {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host = "flickr.com"
        urlComponents.path = "/services/feeds"
        
        return urlComponents
    }
    
    /// https://www.flickr.com/services/feeds/photos_public.gne
    
    static func publicPhotosURL(tags: Set<String>?) -> URL? {
        
        var urlComponents = self.urlComponents
        
        if let tags = tags { urlComponents.queryItems = [URLQueryItem(name: PublicPhotosQueryItemType.tags.rawValue, value: tags.joined(separator: ","))] }
        
        return urlComponents.url?.appendingPathComponent(ResourceType.publicPhotos.rawValue)
    }
}
