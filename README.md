#  HDI - Coding Challenge on 08.12.2019

## Important !!!

Here by I acknowledge that all the characters in the source codes are generated and typed by me and it is a 100% work completed by my own.

## Usage of external dependencies

1. FeedKit https://github.com/nmdias/FeedKit.git

## Assumptions

I assumed that the App's minimum deployment target is iOS 13 and Swift 5.1.

## Architecture

`MVC` is strictly used.

## Known issues

0. The design is tested only on iPad Pro (11- inch). Custom `UICollectionViewCell` sizing is not done
1. Loading UI is not implemented.
2. Some auto-layout warnings appear.
3. No data persistence such as Core Data or SQLite.
4. No Custom UI, simply the iOS built-in UI is used.
5. No comments used, because code speaks better than words.
